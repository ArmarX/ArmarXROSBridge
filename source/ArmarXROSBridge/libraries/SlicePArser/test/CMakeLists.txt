
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore SlicePArser)
 
armarx_add_test(SlicePArserTest SlicePArserTest.cpp "${LIBS}")