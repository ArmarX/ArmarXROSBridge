/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXROSBridge::ArmarXObjects::ROSSubscriberCallback
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <functional>
#include <thread>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <ros/ros.h>

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx
{
    class ROSManager
    {
    public:
        ROSManager(ManagedIceObject& owner);

        void initialize(const std::string& ns = {}, const ros::M_string& remappings = {});
        void startROSLoop(double rate, std::function<void()> fn);
        void stopROSLoop();
        void spinOnce();

    private:
        ManagedIceObject* _owner;
        std::unique_ptr<ros::NodeHandle> _nh;
        std::deque<ros::Subscriber> _subscriber;
        std::deque<ros::Publisher> _publisher;

        std::atomic_bool _stopThread{false};
        std::thread _thread;
    public:
        template<class...Ts>
        ros::Subscriber& subscribe(auto&& ...args)
        {
            ARMARX_CHECK_NOT_NULL(_nh);
            ARMARX_CHECK(!_thread.joinable());
            _subscriber.emplace_back(_nh->subscribe<Ts...>(std::forward<decltype(args)>(args)...));
            return _subscriber.back();
        }
        ros::Subscriber& subscribe(auto&& ...args)
        {
            return subscribe<>(std::forward<decltype(args)>(args)...);
        }

        template<class...Ts>
        ros::Publisher& advertise(auto&& ...args)
        {
            ARMARX_CHECK_NOT_NULL(_nh);
            ARMARX_CHECK(!_thread.joinable());
            _publisher.emplace_back(_nh->advertise<Ts...>(std::forward<decltype(args)>(args)...));
            return _publisher.back();
        }
        ros::Publisher& advertise(auto&& ...args)
        {
            return advertise<>(std::forward<decltype(args)>(args)...);
        }
    };
}
