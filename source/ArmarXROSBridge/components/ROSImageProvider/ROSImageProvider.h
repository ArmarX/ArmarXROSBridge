/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXROSBridge::ArmarXObjects::ROSImageProvider
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/core/ImageProvider.h>
#include <ArmarXROSBridge/libraries/ArmarXROSUtility/ROSManager.h>

namespace armarx
{
    /**
     * @class ROSImageProviderPropertyDefinitions
     * @brief
     */
    class ROSImageProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ROSImageProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ROSTopicImages", "/stereo/left/image_rect_color;/stereo/right/image_rect_color", "Ros topic for rgb images");
            defineOptionalProperty<std::string>("ROSTopicImageDepth", "", "Ros topic for the depth images");

            defineOptionalProperty<double>("MaxFPS", 60, "Frequency used to handle ros messages");


            defineOptionalProperty<Eigen::Vector2i>("Dimensions", Eigen::Vector2i(1280, 960), "")
            .map("320x240", Eigen::Vector2i(320, 240))
            .map("640x480", Eigen::Vector2i(640, 480))
            .map("1280x960", Eigen::Vector2i(1280, 960));
        }
    };

    /**
     * @defgroup Component-ROSImageProvider ROSImageProvider
     * @ingroup ArmarXROSBridge-Components
     * A description of the component ROSImageProvider.
     *
     * @class ROSImageProvider
     * @ingroup Component-ROSImageProvider
     * @brief Brief description of class ROSImageProvider.
     *
     * Detailed description of class ROSImageProvider.
     */
    class ROSImageProvider :
        virtual public visionx::ImageProvider
    {
    public:
        ROSImageProvider() : _ros{*this} {}

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override
        {
            return "ROSImageProvider";
        }

    protected:
        void onInitImageProvider() override;
        void onExitImageProvider() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        ROSManager _ros;
        Ice::Long _imageMinTimestampMs;
    };
}
