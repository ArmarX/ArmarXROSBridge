/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXROSBridge::ArmarXObjects::ROSPointCloudProvider
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/components/pointcloud_core/PointCloudAndImageProvider.h>
#include <ArmarXROSBridge/libraries/ArmarXROSUtility/ROSManager.h>

namespace armarx
{
    /**
     * @class ROSPointCloudProviderPropertyDefinitions
     * @brief
     */
    class ROSPointCloudProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ROSPointCloudProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ROSTopicPointCloud", "/stereo/points2", "Ros topic for the point cloud");

            defineOptionalProperty<std::string>("ROSTopicImages", "/stereo/left/image_rect_color", "Ros topic for rgb images");
            defineOptionalProperty<std::string>("ROSTopicImageDepth", "/stereo/depth", "Ros topic for the depth images");

            defineOptionalProperty<double>("MaxFPS", 60, "Frequency used to handle ros messages");

            defineOptionalProperty<Eigen::Vector2i>("Dimensions", Eigen::Vector2i(1280, 960), "")
            .map("320x240", Eigen::Vector2i(320, 240))
            .map("640x480", Eigen::Vector2i(640, 480))
            .map("1280x960", Eigen::Vector2i(1280, 960));
        }
    };

    /**
     * @defgroup Component-ROSPointCloudProvider ROSPointCloudProvider
     * @ingroup ArmarXROSBridge-Components
     * A description of the component ROSPointCloudProvider.
     *
     * @class ROSPointCloudProvider
     * @ingroup Component-ROSPointCloudProvider
     * @brief Brief description of class ROSPointCloudProvider.
     *
     * Detailed description of class ROSPointCloudProvider.
     */
    class ROSPointCloudProvider :
        virtual public visionx::PointCloudAndImageProvider
    {
    public:
        ROSPointCloudProvider() : _ros{*this} {}

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override
        {
            return "ROSPointCloudProvider";
        }

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitPointCloudAndImageProvider() override;
        void onExitPointCloudAndImageProvider() override;
        void onConnectPointCloudAndImageProvider() override {}
        void onDisconnectPointCloudAndImageProvider() override {}

    protected:
        ROSManager _ros;
        Ice::Long _imageMinTimestampMs;
    };
}
